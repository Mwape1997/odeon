'''
Application of the neuronetwork. An adaptation of the neuronet can be made in the TrainYourOwnYOLO directory
Authors: Mwape, Mehtap, Oana, Andreas
'''

import cv2
import numpy as np
import time
import sys

class OdeonV3:
    def __init__(self):

        self.net = cv2.dnn.readNet("models/yolov3.weights", "models/yolov3.cfg")   # Load Yolo using cv2s deep neuro net fucntion
        self.classes = []   #array for classes of names of the the objects
        with open("models/coco.names", "r") as f:
            self.classes = [line.strip() for line in f.readlines()]     #read line by line for the classes and add them to the array
        layer_names = self.net.getLayerNames()  #get layer names
        self.output_layers = [layer_names[i[0] - 1] for i in self.net.getUnconnectedOutLayers()] #get the output layers from the network
        self.colors = np.random.uniform(0, 255, size=(len(self.classes), 3)) #random "even" numbers between 0 and 255 witht the size of the classes (80)
        OdeonV3.app(self) #call the app function


    def DrawLabel(img, x, y, label, confidence, font=cv2.FONT_HERSHEY_SIMPLEX,  # used for drawing labels.
              font_scale=1, thickness=4): #Takes the frame, the x and y coords of the box where the object is, gets the confidence of that object, gets a font, font scale and font thickness
        size = cv2.getTextSize(label  + "  "+ str(round(confidence, 2)), font, font_scale, thickness)[0]   #size of the text including the confidence.
       # x, y = point
        color = (0, 0, 255)  #color
        point = x, y    #location where to put text.
        cv2.rectangle(img, (x, y - size[1]), (x + size[0], y), color, cv2.FILLED) #draw rectangle underneath the text
        cv2.putText(img, label + " " + str(round(confidence, 2)) + "%", point, font, font_scale, (255, 255, 255), thickness)  #draw the text over the rectangle.

# Detecting objects
    def app(self):
        # Showing informations on the screen
        self.class_ids = [] #get class ids
        self.confidences = []   #confidence of the model on the object
        self.boxes = [] #array containing positioning of boxes
        x = 0   #x coordinate for the box
        y = 0   #y coordinate for the box
        label = ""  #declare a label
        confidence = 0.0    #declare the confidence
        # To capture video from webcam.
        cap = cv2.VideoCapture(0)

        cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)  #set frame width and height of the capture
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
        # To use a video file as input
        # cap = cv2.VideoCapture('filename.mp4')



        font = cv2.FONT_HERSHEY_SIMPLEX
        starting_time = time.time()     #calculate frames
        frame_id = 0
        while True:     #while capture is running (while the webcam is on)
            _, frame = cap.read()   #read input
            #frame_id += 1   #keep track of each frame

            height, width, channels = frame.shape   #image size in numpy array.

            # Detecting objects
            odeon = cv2.dnn.blobFromImage(frame, 0.00392, (160, 160), (0, 0, 0), True, crop=False)


            self.net.setInput(odeon)    #set the webcam as the input for the neuronetwork
            outs = self.net.forward(self.output_layers)     #contains predictions (so what the model thinks the object thinks it is)

            # Showing informations on the screen
            self.class_ids = [] #used to store the class ids
            self.confidences = []   #used to store the confidence
            self.boxes = [] #used to store the boxes that we "draw"
            for out in outs:        #iterate through "outs" (basically the preditions that we calculated above)
                for detection in out:
                    scores = detection[5:]
                    class_id = np.argmax(scores)
                    confidence = scores[class_id]
                    if confidence > 0.6:                            #only get the coords for objects with confidence of at lest 60%
                        # Object detected
                        center_x = int(detection[0] * width)
                        center_y = int(detection[1] * height)
                        w = int(detection[2] * width)
                        h = int(detection[3] * height)

                        # Rectangle coordinates
                        x = int(center_x - w / 2)
                        y = int(center_y - h / 2)

                        self.boxes.append([x, y, w, h])
                        self.confidences.append(float(confidence) * 100)
                        self.class_ids.append(class_id)

            indexes = cv2.dnn.NMSBoxes(self.boxes, self.confidences, 0.8, 0.3) #Performs non maximum suppression given boxes and corresponding scores.

            for i in range(len(self.boxes)):
                if i in indexes:
                    x, y, w, h = self.boxes[i]
                    label = str(self.classes[self.class_ids[i]])    #pass the string of the current detected class to the label variable
                    confidence = self.confidences[i]                #Pass the confidence rating aswell
                    color = self.colors[self.class_ids[i]]          #Give each class id a unique color (deprecated)
                    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 255), 2)
                    #^we tell opencv where to draw the rectangle here and give it a color
                    #(x + w, y + h) is the last coordinate for the rectangle. w is width. h is height
                    '''
                    (x,y)------------
                    -               -
                    -               -
                    -               -
                    -----------------( x + w, y + h)
                    
                    '''

                 #   cv2.putText(frame, label + " " + str(round(confidence, 2)), (x, y + 30), font, 3, color, 3)
                OdeonV3.DrawLabel(frame, x, y, label, confidence) #call the draw label method. Tell it which "img"
                                                                  #to paint on (basically our frame),x,y, the label and the confidence

            elapsed_time = time.time() - starting_time
            fps = frame_id / elapsed_time
            cv2.imshow("Odeon", frame)
            key = cv2.waitKey(1)
            if key == 27:
                break


        cap.release()
        cv2.destroyAllWindows()
        sys.exit(0)


if __name__ == "__main__":
    odeonv3 = OdeonV3()
    odeonv3.app()