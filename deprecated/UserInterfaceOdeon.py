from PyQt5.QtWidgets import QApplication, QPushButton, QMainWindow, QDialog, QGroupBox, QVBoxLayout, QGridLayout, QLabel
import sys
from PyQt5 import QtGui, QtCore
from PyQt5.QtGui import QImage, QPalette, QBrush
from PyQt5.QtCore import QRect, QSize
from PyQt5.QtGui import QPixmap
import App

class Window(QDialog):
    def __init__(self):
        super().__init__()

        self.title = "ODEON - Object Detection"
        self.top = 200
        self.left = 800
        self.width = 1000
        self.height = 700
        self.iconName = "assets/logo.png"

        ...
        # logo = QImage("logo.png")
        # sImage = logo.scaled(QSize(1000, 800))
        # palette = QPalette()
        # palette.setBrush(QPalette.Window, QBrush(sImage))
        # self.setPalette(palette)
        ...

        self.initWindow()

    def initWindow(self):
        self.setWindowTitle(self.title)
        self.setWindowIcon(QtGui.QIcon(self.iconName))
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.createLayout()             #call gridlayout
        vbox = QVBoxLayout()            #create a vboxlayout
        vbox.addWidget(self.groupBox)

        self.setLayout(vbox)

        self.show()

    def createLayout(self):
        self.groupBox = QGroupBox()
        gridLayout = QGridLayout()

        button1 = QPushButton("Start Camera", self)
        button1.setFont(QtGui.QFont("Sanserif", 20))
        # button1.setStyleSheet('background: black') 
        button1.setStyleSheet('color: darkblue')
        button1.setIconSize(QtCore.QSize(30, 30))
        button1.setMinimumHeight(80)
        button1.setMaximumWidth(400)
        gridLayout.addWidget(button1, 0,1) # row 0, column 0
        # button1.setIcon(QtGui.QIcon("logo.png"))

        button2 = QPushButton("About Odeon", self)
        button2.setFont(QtGui.QFont("Sanserif", 20))
        button2.setStyleSheet('color:darkblue')
        button2.setIconSize(QtCore.QSize(30, 30))
        button2.setMinimumHeight(80)
        button1.setMaximumWidth(400)
        gridLayout.addWidget(button2, 1,1)
        # button2.setIcon(QtGui.QIcon("logo.png"))
        self.groupBox.setLayout(gridLayout)

        button1.clicked.connect(self.button1Clicked)  # connected button to Action here
        button2.clicked.connect(self.button2Clicked)

        # Creating a slot (Action)
    def button1Clicked(self):
        print("Button1 Clicked")
        App.OdeonV3().app(self)
        #sys.exit()

    def button2Clicked(self):
        print("Button2 clicked")


if __name__ == "__main__":
    App = QApplication(sys.argv)
    window = Window()
    sys.exit(App.exec())