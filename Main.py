'''
Odeon Main interface. Run this first!
'''

import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout, QGroupBox, QGridLayout, QMessageBox
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QPushButton, QMainWindow, QDialog, QGroupBox, QVBoxLayout, QGridLayout, QLabel
import sys
from PyQt5 import QtGui, QtCore
from qtconsole.qt import QtGui
import App
import Preloader
from threading import Thread
import multiprocessing



class Main(QWidget):

    def __init__(self):
        super().__init__()

        self.title = "ODEON - Object Detection"
        self.top = 200
        self.left = 800
        self.width = 1000
        self.height = 700
        self.iconName = "assets/logo.png"

        ...
        # logo = QImage("logo.png")
        # sImage = logo.scaled(QSize(1000, 800))
        # palette = QPalette()
        # palette.setBrush(QPalette.Window, QBrush(sImage))
        # self.setPalette(palette)
        ...

        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setWindowIcon(QtGui.QIcon(self.iconName))
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.createLayout()             #call gridlayout
        vbox = QVBoxLayout()            #create a vboxlayout
        vbox.addWidget(self.groupBox)

        self.setLayout(vbox)

        self.show()

    def createLayout(self):
        self.groupBox = QGroupBox()
        gridLayout = QGridLayout()

        button1 = QPushButton("Start Camera", self)
        button1.setFont(QtGui.QFont("Sanserif", 20))
        # button1.setStyleSheet('background: black')
        button1.setStyleSheet('color: darkblue')
        button1.setIconSize(QtCore.QSize(30, 30))
        button1.setMinimumHeight(80)
        button1.setMaximumWidth(400)
        gridLayout.addWidget(button1, 0,1) # row 0, column 0
        # button1.setIcon(QtGui.QIcon("logo.png"))

        button2 = QPushButton("About Odeon", self)
        button2.setFont(QtGui.QFont("Sanserif", 20))
        button2.setStyleSheet('color:darkblue')
        button2.setIconSize(QtCore.QSize(30, 30))
        button2.setMinimumHeight(80)
        button1.setMaximumWidth(400)
        gridLayout.addWidget(button2, 1,1)
        # button2.setIcon(QtGui.QIcon("logo.png"))
        self.groupBox.setLayout(gridLayout)

        button1.clicked.connect(self.openOdeon)  # connected button to Action here
        button2.clicked.connect(self.about)

    def openOdeon(self):
        #App.OdeonV3().app(self)
        self.hide()
        p1 = multiprocessing.Process(target=Preloader.main())
        #p2 = multiprocessing.Process(target=App.OdeonV3().app())
        #p2.start()
        p1.start()
        #Thread(target=Preloader.main(), args=()).start()
        #Thread(target=App.OdeonV3().app(), args=()).start()
       # Thread(target=Preloader.main()).start()
       # Thread(target=App.OdeonV3().app()).start()


    def about(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)

        msg.setText("Our Team  was passionate about Machine Learning and created ODEON, an Object detecting software that classify and labels images simply through a laptop camera. Mehtap, Mwape, Andreas and Oana are students at the FH Campus Wien, attending their 3rd Semester in Computer Science and Digital Communication. This project was created in December 2019 as a Software Engineering knowledge implementation. Written in Python and using YOLO, OpenCV, Numpy, TensorFlow, Keras and Microsoft WoTT for labelling. \n\n"
                    "To use ODEON , just press the start button and place objects in front of your laptop camera. In a few seconds you will get them localized and labelled. Have fun detecting!")
        #msg.setInformativeText("This is additional information")
        msg.setWindowTitle("About Odeon")
        msg.setWindowIcon(QtGui.QIcon(self.iconName))
        #msg.setDetailedText("The details are as follows:")
       # msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        #msg.buttonClicked.connect(msgbtn)

        retval = msg.exec_()
        print("value of pressed message box button:", retval)



if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Main()
    sys.exit(app.exec_())