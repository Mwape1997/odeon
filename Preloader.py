import itertools
import threading

import Main
import App
import sys
import time
from PySide2.QtGui import QPixmap
from PySide2.QtWidgets import QApplication, QSplashScreen, QMainWindow
import multiprocessing
from progress.bar import Bar
import progressbar
from tqdm import tqdm

loaded = 0

def main():
    app = QApplication(sys.argv)
    pixmap = QPixmap("assets/logo.png")
    pixmap2 = QPixmap("assets/cube-transparent-loading-gif-9.gif")
    splash = QSplashScreen(pixmap)
    splash.show()
    app.processEvents()
    window = QMainWindow()
    #window.show()
    #splash.finish( & window)
    #time.sleep(10)


    for i in tqdm(range(10000000)):
        if(i == 1):
            print("\nWelcome to Odeon!\n\r")

        if (i == 9999990):
            print("\nImporting modules...\n\r")
            splash.finish(window)
            #pass

        if (i == 9999999):
            print("\nloading opening camera....\n\r")
            print("\nLoading...done\n")
            p1 = multiprocessing.Process(target=App.OdeonV3().app())
            p1.start()
            #pass





    #Main.Main().__init__()
    #App.OdeonV3()

    return app.exec_()

if __name__ == "__main__":
    main()